//
//  Entry.swift
//  GPXParser
//
//  Created by Elis Fokin on 19/07/2019.
//  Copyright © 2019 Elis Fokin. All rights reserved.
//

import Foundation

open class Entry: NSObject {
    var links = [Link]()
    var attributes = [String:String]()
    
    var name: String? {
        set { attributes["name"] = newValue }
        get { return attributes["name"] }
    }
    
    override open var description: String {
        var descriptions = [String]()
        if attributes.count > 0 { descriptions.append("attributes=\(attributes)") }
        if links.count > 0 { descriptions.append("links=\(links)") }
        return descriptions.joined(separator: " ")
    }
}
