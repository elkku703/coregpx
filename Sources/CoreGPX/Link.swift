//
//  Link.swift
//  GPXParser
//
//  Created by Elis Fokin on 19/07/2019.
//  Copyright © 2019 Elis Fokin. All rights reserved.
//

import Foundation

open class Link: CustomStringConvertible {
    public var href: String
    public var linkattributes = [String:String]()
    
    public init(href: String) { self.href = href }
    
    public var url: NSURL? { return NSURL(string: href) }
    public var text: String? { return linkattributes["text"] }
    public var type: String? { return linkattributes["type"] }
    
    public var description: String {
        var descriptions = [String]()
        descriptions.append("href=\(href)")
        if linkattributes.count > 0 { descriptions.append("linkattributes=\(linkattributes)") }
        return "[" + descriptions.joined(separator: " ") + "]"
    }
}
