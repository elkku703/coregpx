//
//  GPXParser.swift
//  GPXParser
//
//  Created by Elis Fokin on 19/07/2019.
//  Copyright © 2019 Elis Fokin. All rights reserved.
//

import Foundation

open class GPXParser: NSObject, XMLParserDelegate {
    
    public typealias GPXCompletionHandler = (GPX?) -> Void
    
    public class func parse(url: URL, completionHandler: @escaping GPXCompletionHandler) {
        GPXParser(url: url, completionHandler: completionHandler).parse()
    }
    
    private var waypoints = [Waypoint]()
    private var tracks = [Track]()
    private var routes = [Track]()
    
    private let url: URL
    private let completionHandler: GPXCompletionHandler
    
    private init(url: URL, completionHandler: @escaping GPXCompletionHandler) {
        self.url = url
        self.completionHandler = completionHandler
    }
    
    private func complete(success: Bool) {
        DispatchQueue.main.async {
            let gpx = GPX()
            gpx.waypoints = self.waypoints
            gpx.tracks = self.tracks
            gpx.routes = self.routes
            self.completionHandler(success ? gpx : nil)
        }
    }
    
    private func fail() { complete(success: false) }
    private func succeed() { complete(success: true) }
    
    private func parse() {
        DispatchQueue.global(qos: .userInitiated).async {
            if let data = try? Data(contentsOf: self.url) {
                let parser = XMLParser(data: data)
                parser.delegate = self
                parser.shouldProcessNamespaces = false
                parser.shouldReportNamespacePrefixes = false
                parser.shouldResolveExternalEntities = false
                parser.parse()
            } else {
                self.fail()
            }
        }
    }
    
    public func parserDidEndDocument(_ parser: XMLParser) { succeed() }
    public func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) { fail() }
    public func parser(_ parser: XMLParser, validationErrorOccurred validationError: Error) { fail() }
    
    private var input = ""
    
    public func parser(_ parser: XMLParser, foundCharacters string: String) {
        input += string
    }
    
    private var waypoint: Waypoint?
    private var track: Track?
    private var link: Link?
    
    public func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        switch elementName {
        case "trkseg":
            if track == nil { fallthrough }
        case "trk":
            tracks.append(Track())
            track = tracks.last
        case "rte":
            routes.append(Track())
            track = routes.last
        case "rtept", "trkpt", "wpt":
            let latitude = NSString(string: attributeDict["lat"]!).doubleValue
            let longitude = NSString(string: attributeDict["lon"]!).doubleValue
            waypoint = Waypoint(latitude: latitude, longitude: longitude)
        case "link":
            link = Link(href: attributeDict["href"]!)
        default: break
        }
    }
    
    public func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        switch elementName {
        case "wpt":
            if waypoint != nil { waypoints.append(waypoint!); waypoint = nil }
        case "trkpt", "rtept":
            if waypoint != nil { track?.fixes.append(waypoint!); waypoint = nil }
        case "trk", "trkseg", "rte":
            track = nil
        case "link":
            if link != nil {
                if waypoint != nil {
                    waypoint!.links.append(link!)
                } else if track != nil {
                    track!.links.append(link!)
                }
            }
            link = nil
        default:
            if link != nil {
                link!.linkattributes[elementName] = input.trimmed
            } else if waypoint != nil {
                waypoint!.attributes[elementName] = input.trimmed
            } else if track != nil {
                track!.attributes[elementName] = input.trimmed
            }
            input = ""
        }
    }
}
