//
//  GPX.swift
//  GPXParser
//
//  Created by Elis Fokin on 19/07/2019.
//  Copyright © 2019 Elis Fokin. All rights reserved.
//

import Foundation

open class GPX {
    
    public var waypoints = [Waypoint]()
    public var tracks = [Track]()
    public var routes = [Track]()
    
    init(waypoints: [Waypoint] = [], tracks: [Track] = [], routes: [Track] = []) {
        self.waypoints = waypoints
        self.tracks = tracks
        self.routes = routes
    }
        
}
