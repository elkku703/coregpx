//
//  String.swift
//  GPXParser
//
//  Created by Elis Fokin on 19/07/2019.
//  Copyright © 2019 Elis Fokin. All rights reserved.
//

import Foundation

extension String {
    var trimmed: String {
        return (self as NSString).trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
    }
    
    var asGpxDate: NSDate? {
        get {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z"
            return dateFormatter.date(from: self) as NSDate?
        }
    }
}
