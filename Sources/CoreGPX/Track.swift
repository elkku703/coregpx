//
//  Track.swift
//  GPXParser
//
//  Created by Elis Fokin on 19/07/2019.
//  Copyright © 2019 Elis Fokin. All rights reserved.
//

import Foundation

open class Track: Entry {
    public var fixes = [Waypoint]()
    
    override open var description: String {
        
        //var a = [1, 2, 3]                                   // [1, 2, 3]
        // var s = a.map{String($0)}.joinWithSeparator(",")    // "1,2,3"
        
        let map = String(fixes.map { $0.description }.joined(separator: "\n"))
        let waypointDescription = "fixes=[\n" + map + "\n]"
        
        
        return [super.description, waypointDescription].joined(separator: " ")
    }
}
