//
//  Waypoint.swift
//  GPXParser
//
//  Created by Elis Fokin on 19/07/2019.
//  Copyright © 2019 Elis Fokin. All rights reserved.
//

import Foundation

open class Waypoint: Entry {
    public var latitude: Double
    public var longitude: Double
    
    init(latitude: Double, longitude: Double) {
        self.latitude = latitude
        self.longitude = longitude
        super.init()
    }
    
    public var info: String? {
        set { attributes["desc"] = newValue }
        get { return attributes["desc"] }
    }
    public lazy var date: NSDate? = self.attributes["time"]?.asGpxDate
    
    override open var description: String {
        return ["lat=\(latitude)", "lon=\(longitude)", super.description].joined(separator: " ")
    }
}
