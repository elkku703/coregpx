import XCTest

import CoreGPXTests

var tests = [XCTestCaseEntry]()
tests += CoreGPXTests.allTests()
XCTMain(tests)
